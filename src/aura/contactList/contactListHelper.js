({
    getContacts: function (component) {
        var accountId = component.get("v.recordId");
        var action = component.get("c.getContacts");

        action.setParams({
            "accountId": accountId
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                try {
                        var allContacts = response.getReturnValue();

                    component.set("v.contactItems", allContacts);
                } catch (error) {
                    console.log(error);
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },

    
    updateContacts: function (component) {
        var contactList = component.get("v.contactItems"); 
        var action = component.get("c.saveContacts");
 
        action.setParams({ 
            "contactList": contactList

        });

        console.log(contactList);

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Result getting Contacts: " + state);
                this.getContacts(component);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);

        component.set("v.isEditable", false);
    },
})