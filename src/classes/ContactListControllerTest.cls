@IsTest
private class ContactListControllerTest {

    @testSetup
    static void setup() {
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();

        for (Integer i = 0; i < 2; i++) {
            accounts.add(new Account(Name = 'anyAccountName' + i));
        }
        insert accounts;

        System.debug('accounts.size() = ' + accounts.size());


        for (Integer i = 0; i < accounts.size(); i++) {
            contacts.add(new Contact(
                    FirstName = 'anyFirstName' + i,
                    LastName = 'anyLastName' + i,
                    Title = 'anyTitle',
                    Email = 'anyemail' + i + '@gmail.com',
                    Accountid = accounts[i].Id
            ));
        }
        insert contacts;
        System.debug('contacts.size() = ' + contacts.size());
    }


    @isTest
    static void testGetContacts() {
        List<Contact> contact = [
                SELECT id, Accountid
                FROM Contact
                WHERE LastName = 'anyLastName0'
        ];

        ID accId = contact[0].AccountId;

        Test.startTest();
        List<Contact> contacts = ContactListController.getContacts(accId);
        Test.stopTest();

        system.assertEquals(1, contacts.size());
        system.assertEquals('anyFirstName0', contacts[0].FirstName);
    }


    @isTest
    static void testSaveContacts() {
        List<Contact> contBefore = [SELECT FirstName FROM Contact WHERE Email = 'anyemail0@gmail.com' LIMIT 1];

        system.assertEquals('anyFirstName0', contBefore[0].FirstName);

        contBefore[0].FirstName = 'newAnyFirstName';

        List<Contact> contAfter = new List<Contact>();

        Test.startTest();
        contAfter = ContactListController.saveContacts(contBefore);
        Test.stopTest();

        system.assertEquals('newAnyFirstName', contAfter[0].FirstName);
    }
}