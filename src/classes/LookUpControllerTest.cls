/**
 * Created by alex on 5/20/2021.
 */
@IsTest
private class LookUpControllerTest {

    @testSetup
    static void setup() {
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();

        for (Integer i = 0; i < 10; i++) {
            accounts.add(new Account(Name = 'anyAccountName' + i));
        }
        insert accounts;

        System.debug('accounts.size() = ' + accounts.size());


        for (Integer i = 0; i < accounts.size(); i++) {
            contacts.add(new Contact(
                    FirstName = 'anyFirstName' + i,
                    LastName = 'anyLastName' + i,
                    Title = 'anyTitle',
                    Email = 'anyemail' + i + '@gmail.com',
                    Accountid = accounts[i].Id
            ));
        }
        insert contacts;
        System.debug('contacts.size() = ' + contacts.size());
    }


    @IsTest
    static void testFetchAccount() {
        String keyWord = 'anyAccountName';
        List <Account> searchLookup = new List<Account>();

        Test.startTest();
        searchLookup = LookUpController.fetchAccount(keyWord);
        Test.stopTest();

        System.assertEquals(10, searchLookup.size());
    }
}