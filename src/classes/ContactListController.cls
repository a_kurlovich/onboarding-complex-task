/**
 * Controller for Aura cmp
 * Selects contacts for View-mode,
 * Upsert contacts for edit-mode
 *
 * UTest: ContactListControllerTest
*/


public with sharing class ContactListController{
	@AuraEnabled
	public static List<Contact> getContacts(Id accountId){
		List<Contact> contactList = [SELECT FirstName, LastName, Title, Email, Accountid, Account.Name
		                             FROM Contact
		                             WHERE AccountId = :accountId];

		return contactList;
	}

	@AuraEnabled
	public static List<Contact> saveContacts(List<Contact> contactList){
		update contactList;
		return contactList;
	}
}