/**
 * Controller for Aura custom cmp - Account lookup
 * Selects lookup Account's  for edit-mode
 *
 * UTest: LookUpControllerTest
*/
public class LookUpController {

    @AuraEnabled
    public static List <Account> fetchAccount(String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        List <Account> returnList = new List <Account>();
        List <Account> lstOfAccount = [SELECT Id, Name FROM Account WHERE Name LIKE :searchKey];

        for (Account acc : lstOfAccount) {
            returnList.add(acc);
        }
        return returnList;
    }
}